import express from 'express';

import fetch from 'isomorphic-fetch';

import Promise from  'bluebird'; // функция Promise.all

import _ from  'lodash'; // функция pick

import cononize from  './cononize';

import fio from  './fio';



const __DEV__ = true;

const app = express()

const baseUrl = 'http://pokeapi.co/api/v2';

const PokemonFields = ['id','name','base_experience','height','is_default','order','weight'];

// pokemon - список всех покемонов
// pokemon/1 - получение конкретного покемона


async function getAllPokemons(url, i = 0){

	console.log('getPokemons', url, i);

	const response = await fetch(url);
			
	const page = await response.json();

	const pokemons = page.results;

	if (__DEV__ && i == 0){

		return pokemons;

	}

	if (page.next){

		const pokemons2 = await getAllPokemons(page.next, i + 1);

		//pokemons <= pokemons2; // добавиьт в первый массив
		
		// тоже самое на ES6 соединяем массивы

		return [
			...pokemons,
			...pokemons2
		];

	}

	return pokemons;

}


async function getPokemon(url){

	console.log('getPokemon', url);

	const response = await fetch(url);
			
	const pokemon = await response.json();

	return pokemon;

}

// const pokemonsUrl = `${baseUrl}/pokemon`;

// getAllPokemons(pokemonsUrl).then(pokemons => { // в корне нельзя использовать await

// 	console.log(pokemons.length);

// }); 


app.get('/', async (req, res) => {

	try {

		const pokemonsUrl = `${baseUrl}/pokemon`;

		const pokemonsInfo = await getAllPokemons(pokemonsUrl);

		const pokemonsPromises = pokemonsInfo.map((info) => {

			return getPokemon(info.url);

		})

		const pokemonsFull = await Promise.all(pokemonsPromises);

		const pokemons = pokemonsFull.map((pokemon) =>{

			return _.pick(pokemon, PokemonFields) // функция lodash


		})

		const sortPokemons = _.sortBy(pokemons, pokemon => pokemon.weight);

		return res.json({sortPokemons});

	} catch(err){

		console.log(err)

		return res.json({err});

	}

});


app.get('/task2A', function (req, res) {

	const a = req.query.a == undefined ? 0 : req.query.a;
	const b = req.query.b == undefined ? 0 : req.query.b;
	const result = a*b;

	console.log(`a: ${a}, b: ${b}`);

	res.json({
		a,
		b,
		result
	});

});


app.get('/task2B', function (req, res) {

	const fullname = req.query.fullname == undefined ? '' : req.query.fullname;

	// console.log(`fullname: ${fullname}`);

	const userfio = fio(fullname);

	if (userfio == undefined) {

		var username = 'Invalid fullname';

	}else{

		var username = userfio.presentation;

	};

	res.json(username);

});

app.get('/task2C', function (req, res) {

	const username = req.query.username == undefined ? '' : req.query.username;

	console.log(`username: ${username}`);

	var userCononize = cononize(username);

	if (userCononize == undefined) {

		userCononize = 'Invalid username';

	};

	console.log(`userCononize: ${userCononize}`);

	res.json({
		username,
		userCononize,
	});

	//res.json(username);

});



app.listen(3000, function () {

 	console.log('Example app listening on port 3000!');

})


//const array = [
//	'https://vk.com/igor.suvorov',
//	'https://twitter.com/suvorovigor',
//	'https://telegram.me/skillbranch',
//	'vk.com/skillbranch/prifile',
//	//'@skillbranch',
//	'https://vk.com/skillbranch?w=wall-117903599_1076'
//];

//array.slice(0,1).forEach(url => {
//
//	const username = cononize(url);
//
//	
//
//});