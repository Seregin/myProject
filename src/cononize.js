export default function canonize(url){

	if (url == '') return undefined;

	var username = url;

	username = username.replace(/@?(https?:)?(\/\/)?/i, "");

	username = username.replace(/(vk\.com)?([^\/]*[\/])?/i, "");

	username = username.replace(/\?.*/i, "");

	if (username != '' && username.search(/[^A-Za-z0-9]/) == -1) {

		return '@' + username;

	}else{

		return undefined;

	}

}