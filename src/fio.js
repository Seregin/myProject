export default function fio(fullname){

	if (fullname == undefined) return undefined; 

	const arr = fullname.split(' ');

	var family = undefined;

	var name = undefined;

	var patronymic = undefined;

	if (arr.length == 3){

		family = String(arr[2]);

		name = String(arr[0]);

		patronymic = String(arr[1]);

	}else if (arr.length == 2) {

		family = String(arr[1]);
		
		name = String(arr[0]);

	}else if (arr.length == 1) {

		family = String(arr[0]);

	}else{

		return undefined;	

	}

	//..

	const n = name == undefined ? '' : name.substr(0,1) + '.';

	const p = patronymic == undefined ? '' : patronymic.substr(0,1) + '.';

	var presentation
	 	 = family
	 	 + (n == '' ? '': ' ' + n)
	 	 + (p == '' ? '': ' ' + p);

	const fio = {
		fullname,
		family,
		name,
		n,
		patronymic,
		p,
		presentation,
	}

	console.log(fio);

	return fio;

}